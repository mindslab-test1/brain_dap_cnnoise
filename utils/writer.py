import numpy as np
from torch.utils.tensorboard import SummaryWriter

from .plotting import plot_spectrogram_to_numpy


class MyWriter(SummaryWriter):
    def __init__(self, hp, logdir):
        super(MyWriter, self).__init__(logdir)
        self.hp = hp

    def log_training(self, train_loss, step):
        self.add_scalar('train_loss', train_loss, step)

    def log_evaluation(self, test_loss, ssnr,
        clean, result, noisy,
        noisy_spec, clean_spec, est_spec, est_mask,
        step):
        self.add_scalar('test_loss', test_loss, step)
        self.add_scalar('ssnr', ssnr, step)

        self.add_audio('clean_audio', clean, step, self.hp.audio.sr)
        self.add_audio('result_audio', result, step, self.hp.audio.sr)
        self.add_audio('noisy_audio', noisy, step, self.hp.audio.sr)

        self.add_image('data/noisy_spectrogram',
            plot_spectrogram_to_numpy(noisy_spec), step, dataformats='HWC')
        self.add_image('data/target_spectrogram',
            plot_spectrogram_to_numpy(clean_spec), step, dataformats='HWC')
        self.add_image('result/estimated_spectrogram',
            plot_spectrogram_to_numpy(est_spec), step, dataformats='HWC')
        self.add_image('result/estimated_mask',
            plot_spectrogram_to_numpy(est_mask), step, dataformats='HWC')
        self.add_image('result/estimation_error_abs',
            plot_spectrogram_to_numpy(np.abs(est_spec - clean_spec)), step, dataformats='HWC')
