#!/usr/bin/env python
import os
import glob
import tqdm
import torch
import librosa
import argparse
import numpy as np

from utils.audio import Audio
from utils.hparams import HParam
from utils.utils import read_wav
from model.model import CNNoise


def main(args, hp):
    model = CNNoise(hp).cuda()
    chkpt_model = torch.load(args.checkpoint_path)['model']
    model.load_state_dict(chkpt_model)
    model.eval()

    audio = Audio(hp)

    def apply_cnnoise_part(wav):
        wav = torch.tensor(wav)
        wav = wav.cuda().float()
        wav = wav.unsqueeze(0)
        mag, phase = model.get_mag_phase(wav)

        est_mask = model(mag)
        est_mag = mag * est_mask
        est_mag = model.post_spec(est_mag)

        est_mag = est_mag[0].cpu().detach().numpy()
        phase = phase[0].cpu().detach().numpy()
        est_wav = audio.spec2wav(est_mag, phase)
        return est_wav

    def apply_cnnoise(wav):
        wavlen = len(wav)
        max_chunk_size = hp.audio.sr * 3600 # max 3600 seconds = 1hours
        segment_num = (wavlen // max_chunk_size) + 1
        result = list()

        for wav_part in np.array_split(wav, segment_num):
            result_wav = apply_cnnoise_part(wav_part)
            result.append(result_wav)

        result = np.concatenate(result)
        return result

    with torch.no_grad():
        for wavpath in tqdm.tqdm(glob.glob(os.path.join(args.input_dir, '*.wav'))):
            sr, wav = read_wav(wavpath)
            if sr != hp.audio.sr:
                wav = librosa.resample(wav, sr, hp.audio.sr)
                wav = np.clip(wav, -1.0, 1.0)

            result = apply_cnnoise(wav)

            os.makedirs(args.out_dir, exist_ok=True)
            filename = os.path.basename(wavpath)
            out_path = os.path.join(args.out_dir, filename)
            librosa.output.write_wav(out_path, result, sr=hp.audio.sr)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="yaml file for configuration")
    parser.add_argument('-p', '--checkpoint_path', type=str, default=True,
                        help="path of checkpoint pt file")
    parser.add_argument('-i', '--input_dir', type=str, required=True,
                        help='directory of mixed wav file')
    parser.add_argument('-o', '--out_dir', type=str, required=True,
                        help='directory of output')

    args = parser.parse_args()
    hp = HParam(args.config)

    main(args, hp)
    print(torch.cuda.max_memory_allocated(device=0))
