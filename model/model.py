import torch
import torch.nn as nn
import torch.nn.functional as F


class CNNoise(nn.Module):
    def __init__(self, hp):
        super(CNNoise, self).__init__()
        self.hp = hp

        self.pre_conv = nn.Sequential(
            nn.Conv1d(hp.audio.num_freq, 256, kernel_size=1),
            nn.BatchNorm1d(256), nn.LeakyReLU(),
        )
        self.tconv = nn.Sequential(
            nn.Conv1d(256, 256, kernel_size=5, padding=2, dilation=1, groups=256),
            nn.Conv1d(256, 256, kernel_size=1),
            nn.BatchNorm1d(256), nn.LeakyReLU(),

            nn.Conv1d(256, 256, kernel_size=5, padding=4, dilation=2, groups=256),
            nn.Conv1d(256, 256, kernel_size=1),
            nn.BatchNorm1d(256), nn.LeakyReLU(),

            nn.Conv1d(256, 256, kernel_size=5, padding=8, dilation=4, groups=256),
            nn.Conv1d(256, 256, kernel_size=1),
            nn.BatchNorm1d(256), nn.LeakyReLU(),

            nn.Conv1d(256, 256, kernel_size=5, padding=16, dilation=8, groups=256),
            nn.Conv1d(256, 256, kernel_size=1),
            nn.BatchNorm1d(256), nn.LeakyReLU(),

            nn.Conv1d(256, 256, kernel_size=5, padding=32, dilation=16, groups=256),
            nn.Conv1d(256, 256, kernel_size=1),
            nn.BatchNorm1d(256), nn.LeakyReLU(),
        )

        self.fc = nn.Sequential(
            nn.Linear(256, 256),
            nn.LeakyReLU(),
            nn.Linear(256, 256),
            nn.LeakyReLU(),
            nn.Linear(256, 256),
            nn.LeakyReLU(),
            nn.Linear(256, hp.audio.num_freq),
            nn.LeakyReLU(),
            nn.Linear(hp.audio.num_freq, hp.audio.num_freq),
        )

        self.register_buffer('window', torch.hann_window(window_length=hp.audio.win_length))

    def get_mag_phase(self, x):
        x = torch.stft(x, n_fft=self.hp.audio.n_fft,
                       hop_length=self.hp.audio.hop_length,
                       win_length=self.hp.audio.win_length,
                       window=self.window) # [B, num_freq, T, 2]
        mag = torch.norm(x, p=2, dim=-1) # [B, num_freq, T]
        mag = self.pre_spec(mag)
        phase = torch.atan2(x[:, :, :, 1], x[:, :, :, 0]) # [B, num_freq, T]
        return mag, phase

    def pre_spec(self, x):
        return self.normalize(self.amp_to_db(x) - self.hp.audio.ref_level_db)

    def post_spec(self, x):
        return self.db_to_amp(self.denormalize(x) + self.hp.audio.ref_level_db)

    def amp_to_db(self, x):
        return 20.0 * torch.log10(torch.max(torch.tensor([1e-5]).cuda(), x))

    def db_to_amp(self, x):
        return torch.pow(torch.tensor([10.0]).cuda(), 0.05*x)

    def normalize(self, x):
        return torch.clamp(x / -self.hp.audio.min_level_db, -1.0, 0.0) + 1.0

    def denormalize(self, x):
        return (torch.clamp(x, 0.0, 1.0) - 1.0) * -self.hp.audio.min_level_db

    def forward(self, x):
        # [B, num_freq, T]
        x = self.pre_conv(x) # [B, 256, T]
        x = x + self.tconv(x) # [B, 256, T]
        x = x.transpose(1, 2).contiguous() # [B, T, 256]
        x = self.fc(x) # [B, T, num_freq]
        x = torch.sigmoid(x)
        x = x.transpose(1, 2) # [B, num_freq, T]
        return x
