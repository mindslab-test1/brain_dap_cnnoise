import os
import glob
import torch
import random
import numpy as np
from torch.utils.data import Dataset, DataLoader

from utils.utils import read_wav_np, cut_wav


def create_dataloader(hp, args, train):
    if train:
        return DataLoader(dataset=NoiseDataset(hp, args, True),
                          batch_size=hp.train.batch_size,
                          shuffle=True,
                          num_workers=hp.train.num_workers,
                          pin_memory=True,
                          drop_last=True)
    else:
        return DataLoader(dataset=NoiseDataset(hp, args, False),
                          batch_size=hp.train.batch_size,
                          shuffle=False,
                          num_workers=hp.train.num_workers,
                          pin_memory=True,
                          drop_last=False)


class NoiseDataset(Dataset):
    def __init__(self, hp, args, train):
        self.hp = hp
        self.args = args
        self.train = train
        self.clean_dir_list = hp.data.clean
        self.noise_dir_list = hp.data.noise
        self.music_dir_list = hp.data.music
        self.sample_len = int(hp.audio.sr * hp.data.audio_len)

        def find_all(root_list):
            temp = list()
            for root in root_list:
                temp.extend(glob.glob(os.path.join(root, '**', '*.wav'), recursive=True))
            random.shuffle(temp)
            return temp

        self.clean_wav_list = find_all(self.clean_dir_list)
        self.noise_wav_list = find_all(self.noise_dir_list)
        self.music_wav_list = find_all(self.music_dir_list)

        # don't shuffle clean_wav_list in order to use unseen speakers when testing
        # of course, only one speaker will be used for both train/test
        random.seed(123)
        random.shuffle(self.noise_wav_list)
        random.seed(123)
        random.shuffle(self.music_wav_list)

        if train:
            self.clean_wav_list = self.clean_wav_list[:int(0.9*len(self.clean_wav_list))]
            self.noise_wav_list = self.noise_wav_list[:int(0.9*len(self.noise_wav_list))]
            self.music_wav_list = self.music_wav_list[:int(0.9*len(self.music_wav_list))]
        else:
            self.clean_wav_list = self.clean_wav_list[int(0.9*len(self.clean_wav_list)):]
            self.noise_wav_list = self.noise_wav_list[int(0.9*len(self.noise_wav_list)):]
            self.music_wav_list = self.music_wav_list[int(0.9*len(self.music_wav_list)):]


    def __len__(self):
        return self.hp.data.steps_per_epoch * self.hp.train.batch_size

    def __getitem__(self, idx):
        clean = random.choice(self.clean_wav_list)
        if random.uniform(0, 1) < self.hp.data.noise_vs_music:
            noise = random.choice(self.noise_wav_list)
        else:
            noise = random.choice(self.music_wav_list)

        sr, clean_wav = read_wav_np(clean)
        assert sr == self.hp.audio.sr, 'sample rate should be consistent with config'
        sr, noise_wav = read_wav_np(noise)
        assert sr == self.hp.audio.sr, 'sample rate should be consistent with config'

        clean_wav = cut_wav(self.sample_len, clean_wav)
        noise_wav = cut_wav(self.sample_len, noise_wav)
        assert len(clean_wav) == len(noise_wav), "length mismatch even after preprocessing"

        snr = np.random.normal(self.hp.data.snr.mean, self.hp.data.snr.std)
        noise_wav *= 10 ** (-snr/10)

        noisy_wav = clean_wav + noise_wav

        norm = np.max(np.abs(np.concatenate((noisy_wav, noise_wav, clean_wav))))
        noisy_wav /= norm
        noise_wav /= norm
        clean_wav /= norm

        noisy_wav = torch.from_numpy(noisy_wav)
        noise_wav = torch.from_numpy(noise_wav)
        clean_wav = torch.from_numpy(clean_wav)

        return clean_wav, noisy_wav
